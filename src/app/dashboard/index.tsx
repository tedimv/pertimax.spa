import React, { useContext, useState } from 'react';
import { makeAutoObservable, toJS } from 'mobx';
import { observer, useLocalObservable } from 'mobx-react';

import Modal from '../../components/modals';
import { NotificationContext, NotificationTypes } from '../../contexts/notification/index';
import { applyLoader, Loader } from '../../models/loader';
import { User } from '../../models/user';
import { fetchUsers, getUser } from '../../ api/users';
import { EntityList, TEntityList } from '../../models';
import ShowUser from './ShowUser';


interface TDashboardState {
	users: Loader<TEntityList<User>>
}

class DashboardState implements TDashboardState {
	users = new Loader<TEntityList<User>>(() => new EntityList({ Ctor: User }));
	single = new Loader<User>(() => new User());
	
	constructor() {
		makeAutoObservable(this, {}, { autoBind: true });
		applyLoader({ obj: this, prop: 'users', fn: fetchUsers, single: false, Ctor: User });
		applyLoader({ obj: this, prop: 'single', fn: getUser, single: true, Ctor: User });
	}
}

function Dashboard() {
	const { pushNotification } = useContext(NotificationContext);
	const state = useLocalObservable(() => new DashboardState());
	
	console.log(toJS(state))
	// const [ modalOpen, setModalOpen ] = useState(false);
	//
	//
	// function toggleModal() {
	// 	setModalOpen(!modalOpen);
	// }
	//
	// function createRandomNotification() {
	// 	let type: NotificationTypes;
	// 	const random = Math.random() * 10;
	// 	if(random <= 4) type = NotificationTypes.info;
	// 	else if(random <= 7) type = NotificationTypes.warning;
	// 	else type = NotificationTypes.error;
	// 	pushNotification(type, 'TEST NOTIFICATION');
	// }
	
	return (
		<div>
			Dashboard
			
			{!state.users.isLoading && state.users.data.entities.map(item => (
				<ShowUser key={item._id} item={item}/>
			))}
			{/*<button*/}
			{/*	onClick={toggleModal}*/}
			{/*	style={{ marginBottom: 50 }}>*/}
			{/*	Open modal*/}
			{/*</button>*/}
			
			{/*<button*/}
			{/*	onClick={createRandomNotification}*/}
			{/*	style={{ marginBottom: 50 }}>*/}
			{/*	Add notification*/}
			{/*</button>*/}
			{/*<Modal*/}
			{/*	backdrop={false}*/}
			{/*	open={modalOpen}*/}
			{/*	onClose={toggleModal}>*/}
			{/*	<div>haj</div>*/}
			{/*</Modal>*/}
		
		</div>
	);
}

export default observer(Dashboard);
