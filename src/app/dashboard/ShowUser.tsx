import React from 'react';

import { User } from '../../models/user';

type ShowUserProps = {
	item: User
}

function ShowUser({ item }: ShowUserProps) {
	
	return (
		<div>
			<div>{item.firstName || 'N/A'}</div>
			<div>{item.familyName}</div>
			<div>{item.email}</div>
			<div>{item._id}</div>
			
			<hr />
			
			<input
				value={item.firstName}
				onChange={e => item.set('firstName', e.target.value)} />
		</div>
	);
}

export default ShowUser;
