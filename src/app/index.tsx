import React from 'react';
import { Switch, Route } from 'react-router';

import Dashboard from './dashboard';
import AuthProvider from '../auth/AuthProvider';
import Login from '../auth/Login';

function Main() {
	return (
		<div className='App'>
			<AuthProvider>
				<Login/>
				<Switch>
					<Route
						path='/'
						exact
						component={Dashboard} />
				</Switch>
			</AuthProvider>
		</div>
	);
}

export default Main;
