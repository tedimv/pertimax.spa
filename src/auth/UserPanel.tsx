import React from 'react';
import { userDto, UserT } from '../models/user';
import useDto from '../hooks/useDto';
import { DtoActionTypes } from '../models/dto';

function UserPanel(user: UserT) {
	const { state, updateDto } = useDto<UserT, DtoActionTypes>(userDto, user);
	
	return (
		<div>{state.name}</div>
	);
}

export default UserPanel;
