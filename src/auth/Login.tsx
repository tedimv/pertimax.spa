import React from 'react';
import GoogleLogin, { GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login';
import axios from 'axios';
import moment from 'moment';

const clientId = '426757195360-rppllpuc9bm8ioshmpebboubh0if849h.apps.googleusercontent.com';

function Login(props: any) {
	
	async function loginSuccess(googleUser: GoogleLoginResponse | GoogleLoginResponseOffline) {
		if('getAuthResponse' in googleUser) {
			const response = googleUser.getAuthResponse();
			
			const res = await axios.post('http://localhost:5000/login', { token: response['id_token'] }, {
				headers: { 'Access-Control-Allow-Origin': '*' }
			});
			console.log(res)
			// @ts-ignore
			console.log(moment(res['expires_at'], 'DD-MM-YYYY, HH:mm'));
		}
	}
	
	return (
		
		<GoogleLogin
			clientId={clientId}
			onSuccess={loginSuccess}
			buttonText='Login With Google'
			className='google-login'
			scope='profile'
			fetchBasicProfile={false}
		/>
	);
}

export default Login;
