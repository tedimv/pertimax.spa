import React, { createContext, useState } from 'react';

import { ChildrenProp } from '../models';


const AuthContext = createContext<any>({});

function AuthProvider({ children }: ChildrenProp) {
	const [user, setUser] = useState({});
	
	function login(...args: any) {
		console.log(args);
	}
	
	return (
		<AuthContext.Provider value={{
			user,
			login
		}}>
			{children}
		</AuthContext.Provider>
	);
}

export default AuthProvider;
