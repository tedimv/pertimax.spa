import axios from 'axios';
import { makeAutoObservable, flow, toJS,  action} from 'mobx';

interface Loader<T> {
	loading: boolean,
	data: T | null
}

interface IList<T> {
	totalCount: number,
	entities: T[]
}

const test = 123;

export default test;

