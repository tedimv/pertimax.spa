import { ReactNode } from 'react';
import { makeAutoObservable } from 'mobx';
import { ILoader } from './loader';

export type ChildrenProp = { children: ReactNode };
export type TCtor<T> = new (...props: any) => T
export type TEntity = { _id: string | null, __v: number };
export type TEntityList<T> = { totalCount: number, entities: T[] }
export type TEntityListProps<T> = { Ctor?: new (props: any) => T, data?: T[], totalCount?: number }

export function createSetter<T>(obj: any = {}) {
	return (prop: keyof T, value: unknown) => {
		obj[prop] = value;
	};
}

export function updateObj(obj: any = {}) {
	return (updated: any) => {
		obj = { ...obj, ...updated };
	};
}

export class EntityList<T> implements TEntityList<T> {
	entities: T[] = [];
	totalCount = 0;
	
	constructor(props: TEntityListProps<T> = {}) {
		const {
			Ctor,
			data = [],
			totalCount = 0
		} = props;
		
		makeAutoObservable(this, {}, { autoBind: true });
		if (Ctor) {
			this.entities = data.map(item => new Ctor(item));
			this.totalCount = totalCount;
		}
	}
}

