export type DividerProps = {
    orientation: 'horizontal' | 'vertical',
    height?: string,
    width?: string,
    color?: string,
    thickness?: number,
    margin?: string
}