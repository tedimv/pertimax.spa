import { makeAutoObservable } from 'mobx';

import { dtoBaseReducer, dtoBaseFactory } from '../generics';
import { DtoActionTypes, DtoSetup, DtoUnique } from './dto';
import { createSetter, TEntity } from './index';

export type UserT = DtoUnique & {
	name: string,
	age: number | null,
}


const defaultUser: UserT = {
	name: 'New User',
	age: null,
	id: null
};

export const userDto: DtoSetup<UserT, DtoActionTypes> = {
	reducer: dtoBaseReducer<UserT>(defaultUser),
	factory: dtoBaseFactory<UserT>(defaultUser)
};



export type TUser = TEntity & {
	firstName: string
	familyName: string
	email: string
}

export type TLoginSuccess = TUser & {
	token: string
}

export class User implements TUser {
	__v = 0;
	_id = null;
	firstName = 'Test';
	familyName = 'User';
	email = '';
	
	constructor(props?: TUser) {
		makeAutoObservable(this, {}, { autoBind: true });
		Object.assign(this, props);
	}
	
	set = createSetter<TUser>(this);
}


