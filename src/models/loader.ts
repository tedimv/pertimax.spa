import { AxiosResponse } from 'axios';

import { TCtor, TEntityList } from './index';
import { makeAutoObservable } from 'mobx';

export interface ILoader<T> {
	isLoading: boolean
	data: T | TEntityList<T> | undefined,
	error?: string | null
}


export type TAxiosPromise<T> = Promise<TApiData<T>>
export type TApiData<T> = AxiosResponse<null | T | TEntityList<T>>
export type TApiFn<T> = (args?: any) => Promise<TApiData<T>>;

export class Loader<T> implements ILoader<T> {
	data;
	isLoading = true;
	error = null;
	
	constructor(init: () => T) {
		makeAutoObservable(this, {}, { autoBind: true });
		this.data = init();
	}
}


interface TApplyLoaderProps<T> {
	obj: any,
	prop: string,
	fn: TApiFn<T>,
	single: boolean,
	Ctor: TCtor<T>
}

export function applyLoader<T>(args: TApplyLoaderProps<T>) {
	const { Ctor, fn, obj, prop, single } = args;
	
	function shapeResponse(data: any): T | TEntityList<T> {
		return single ?
			new Ctor(data) : {
				entities: data.entities.map((item: T) => new Ctor(item)),
				totalCount: data.totalCount
			};
	}
	
	(async() => {
		try {
			const response = await fn();
			obj[prop] = { ...obj[prop], data: shapeResponse(response.data), isLoading: false };
		} catch (error) {
			obj[prop] = { ...obj[prop], error, isLoading: false };
		}
	})();
}

