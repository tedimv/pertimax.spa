export interface IconContainerProps {
    side?: number,
    imgUrl: string,
    backgroundColor?: string
}

export interface IconComponentProps extends IconContainerProps {
    onClick: () => unknown
}