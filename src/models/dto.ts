export type DtoUnique = { id: number | string | null }
export type DtoReducer<T, E> = (state: T, action: DtoAction<T, E>) => T;
export type DtoReader<T> = (item?: T) => T;
export type DtoSetup<T, E> = {
	reducer: DtoReducer<T, E>,
	factory: DtoReader<T>
}

export enum DtoActionTypes {
	update = 'update',
}

export type DtoAction<T, E> = {
	type: E,
	property: keyof T,
	payload: any,
	meta?: { [key: string]: string | number }
}

export type DtoKey<T> = keyof T;
