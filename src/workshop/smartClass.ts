
type Street = {
	name: string,
	coordinates: number[]
}

type TAddress = {
	id: number | null
	country: string
	streets: Street[]
}


type TPerson = {
	name: string,
	addresses: TAddress[]
}

const defaultAddress : TAddress = {
	country: 'Bulgaria',
	streets: [],
	id: null
}

function dec() {
	return (obj: any, key: string) : any => {
		function getter() {
			console.log()
			// @ts-ignore
			return this[key];
		}
		
		Object.defineProperty(obj, key, {
		
		});
	}
}

function complexParam(items: Street[]) : Street[]{
	return items;
}

class Address implements TAddress {
	id;
	country;
	@dec() streets;
	
	constructor(params: TAddress = defaultAddress) {
		const {
			id,
			streets,
			country
		} = params;
		
		this.id = id;
		this.streets = streets;
		this.country = country;
	}
}


class Person implements TPerson {
	name = '';
	addresses = [];
	
	constructor(props: any) {
	
	}
}


export default {}
