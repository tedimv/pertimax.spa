import React, { CSSProperties, useState } from 'react';

import { Notification, NotificationTypes } from './index';

type CardProps = {
	item: Notification,
	onClose: () => void
}

function getCardStyles(type: NotificationTypes): CSSProperties {
	const baseStyles: CSSProperties = {
		height: '3rem',
		width: '15rem',
		display: 'flex',
		alignItems: 'center',
		padding: '0.5rem',
		fontSize: '0.9em',
		cursor: 'pointer'
	};
	
	switch (type) {
		case NotificationTypes.info:
			return { backgroundColor: 'rgba(157,243,140)', ...baseStyles };
		case NotificationTypes.warning:
			return { backgroundColor: 'rgb(250,220,82)', ...baseStyles };
		case NotificationTypes.error:
			return { backgroundColor: 'rgb(255,113,113)', ...baseStyles };
		default:
			return { backgroundColor: 'rgb(255,255,255)', ...baseStyles };
	}
}

function Card({ item, onClose }: CardProps) {
	const { type, message } = item;
	
	const [ remaining, setRemaining ] = useState(1);
	
	return (
		<div
			style={getCardStyles(type)}
			onClick={onClose}>
			{message}
		</div>
	);
}


export default Card;
