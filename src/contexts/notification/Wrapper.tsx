import React, { ReactNode } from 'react';

type WrapperProps = {
	children: ReactNode[]
}

function Wrapper({ children }: WrapperProps) {
	return (
		<div id='notifications-wrapper' style={{
			position: 'absolute',
			bottom: '20px',
			right: '20px',
			display: 'grid',
			gridTemplateRows: 'repeat(auto, 3em)',
			rowGap: '1em'
		}}>
			{children}
		</div>
	);
}

export default Wrapper;
