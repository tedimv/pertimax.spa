import React, { createContext, ReactNode, useState } from 'react';

import Portal from '../../components/portal';
import Card from './Card';
import Wrapper from './Wrapper';

type NotificationProps = {
	children: ReactNode
}

type TNotificationContext = {
	pushNotification(type: NotificationTypes, message: string): void,
	notifications: Notification[]
}

export type Notification = {
	type: NotificationTypes,
	message: string
}

export enum NotificationTypes {
	info = 'info',
	warning = 'warning',
	error = 'error'
}

export const NotificationContext = createContext<TNotificationContext>({
	pushNotification: () => void(0),
	notifications: []
});

function NotificationProvider({ children }: NotificationProps) {
	const [ notificationStack, setNotificationStack ] = useState<Notification[]>([]);
	
	function pushNotification(type: NotificationTypes, message: string) {
		setNotificationStack(prev => [ ...prev, { type, message } ]);
	}
	
	function closeNotification(index: number) {
		return () => setNotificationStack(prev => prev.filter((_, i) => i !== index));
	}

	return (
		<NotificationContext.Provider
			value={{
				notifications: notificationStack,
				pushNotification
			}}>
			{children}
			<Portal
				open
				type='notifications'>
				<Wrapper>
				{notificationStack.map((item, i) => (
					<Card
						key={`notification-${i}-${item.message}`}
						item={item}
						onClose={closeNotification(i)} />
				))}
				</Wrapper>
			</Portal>
		</NotificationContext.Provider>
	);
}

export default NotificationProvider;
