import React, { useEffect, useReducer } from 'react';
import { authReducer, initAuth } from './reducer';

function useAuth() {
	const [state, reducer] = useReducer(authReducer, initAuth);
	
	useEffect(() => {
	
	}, []);
	
	return {
	
	};
}

export default useAuth;
