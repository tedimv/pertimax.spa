export type AuthState = {
	firstName: string,
	familyName: string,
	email: string,
	_id: string,
	token: string
}

type AuthAction = {
	type: AuthActionTypes,
	payload: AuthState
}

enum AuthActionTypes {
	login,
	logout
}

export const initAuth: AuthState = {
	_id: '',
	email: '',
	familyName: '',
	firstName: '',
	token: ''
};

export function authReducer(state = initAuth, action: AuthAction) {
	switch (action.type) {
		case AuthActionTypes.login:
			localStorage.setItem('pertimax', JSON.stringify(action.payload));
			return action.payload;
		case AuthActionTypes.logout:
			localStorage.removeItem('pertimax');
			return initAuth;
		default:
			return state;
	}
}

