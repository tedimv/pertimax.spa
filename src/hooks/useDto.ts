import { useReducer } from 'react';
import { DtoKey, DtoSetup } from '../models/dto';


function useDto<T, E>(dtoSetup: DtoSetup<T, E>, item?: T) {
	const [ state, dispatch ] = useReducer(dtoSetup.reducer, dtoSetup.factory(item));
	
	function updateDto(type: E, key: DtoKey<T>, value: any): void {
		dispatch({ type, property: key, payload: value });
	}
	
	return { state, updateDto };
}

export default useDto;
