import styled from 'styled-components';
import { animated } from 'react-spring';

import AppBackground from '../assets/images/background_4k.jpg';



export const AppContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    position: relative;
`;

export const AppContent = styled.div`
    width: calc(100% - 64px);
    height: 100%;
    background-image: url(${AppBackground});
    background-size: cover;
`;

export const AppSidebar = styled(animated.div)<any>`
    display: flex;
    flex-direction: column;
    padding: 15px 0;
    border: 1px solid lightgrey;
    box-sizing: border-box;
    align-items: center;
    background-color: #fff;
`
