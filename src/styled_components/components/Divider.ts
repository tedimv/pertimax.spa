import styled from 'styled-components';

import { DividerProps } from '../../models/Divider';


// border-${({ orientation }) => orientation === 'horizontal' ? 'bottom' : 'right'}: ${({ thickness }) => thickness || 1}px ${({ borderStyle }) => borderStyle || 'solid'} ${({ color }) => color || 'gray'};
//  
// ${({ orientation, height, width }) => orientation === 'horizontal' ? `width: ${width || '100%'}` : `height: ${height || '100%'}`};    

export const StyledDivider = styled.hr<DividerProps>`
    transform: rotate(${({ orientation }) => orientation === 'horizontal' ? 0 : 90}deg);
    margin: ${({ margin }) => margin || '0'};
    width: ${({ width }) => width || '100%'};
    height: ${({height}) => height || '2px'};
    background-color: ${({color}) => color || 'gray'};
    box-sizing: border-box;
    border: none;
`