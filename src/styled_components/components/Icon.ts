import styled from 'styled-components';

import { IconContainerProps } from '../../models/Icon';

export const IconContainer = styled.div<IconContainerProps>`
    height: ${props => props.side || 26}px;
    width: ${props => props.side || 26}px;
    background-image: url(${props => props.imgUrl});
    background-color: ${props => props.backgroundColor || 'none'};
    cursor: pointer;
`;