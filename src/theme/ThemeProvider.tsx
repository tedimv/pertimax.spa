import React, { createContext, ReactNode, useState } from 'react';

import { ThemeContext, ThemePreset } from './theme';
import { themePresets } from './presets';

type Props = {
  children: ReactNode
}

export const AppThemeContext = createContext<ThemeContext>({
  theme: themePresets[0],
  setActiveTheme: () => undefined,
});


function ThemeProvider({ children }: Props) {
  const [ theme, setTheme ] = useState<ThemePreset>(themePresets[0]);
  
  function setActiveTheme(index: number) {
    setTheme(themePresets[index]);
  }
  
  return (
    <AppThemeContext.Provider
      value={{
        theme,
        setActiveTheme,
      }}>
      {children}
    </AppThemeContext.Provider>
  );
}

export default ThemeProvider;
