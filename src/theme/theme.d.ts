import { CSSProperties } from 'react';

type ThemePreset = {
  name: string,
  [prop: string]: string | CSSProperties
}

type ThemeContext = {
  theme: ThemePreset,
  setActiveTheme: (index: number) => void
}
