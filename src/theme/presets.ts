import { ThemePreset } from './theme';

export const themePresets: ThemePreset[] = [
  {
    name: 'light',
    colors: {
      backgroundColor: '#e9e9e9',
      color: '#353535'
    },
    header: {
      backgroundColor: '#fff',
      borderRadius: '5px 5px 0px 0px'
    }
  }
];
