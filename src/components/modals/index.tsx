import React, { ReactNode } from 'react';

import Portal from '../portal';
import Wrapper from './Wrapper';

type ModalProps = {
	styles?: CSSStyleSheet,
	children: ReactNode,
	backdrop?: boolean,
	open: boolean,
	onClose: () => unknown
}

function Modal({ open = false, children, backdrop = true, onClose }: ModalProps) {
	
	return (
		<Portal
			type='modals'
			open={open}>
			<Wrapper
				backdrop={backdrop}
				onClose={onClose}>
				{children}
			</Wrapper>
		</Portal>
	);
}

export default Modal;
