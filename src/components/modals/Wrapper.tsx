import React, { ReactNode, CSSProperties, useMemo } from 'react';

type WrapperProps = {
	children: ReactNode,
	backdrop: boolean,
	onClose: () => unknown
}

function Wrapper({ children, onClose, backdrop }: WrapperProps) {
	const wrapperStyles = useMemo((): CSSProperties => {
		return {
			position: 'absolute',
			top: 0,
			left: 0,
			height: '100%',
			width: '100%',
			background: backdrop ? 'rgb(45 45 45 / 50%)' : 'none',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center'
		};
	}, [ backdrop ]);
	
	return (
		<div
			id='modal-wrapper'
			style={wrapperStyles}
			onClick={onClose}>
			{children}
		</div>
	);
}

export default Wrapper;
