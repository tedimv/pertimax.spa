import React from 'react';

import { IconComponentProps } from '../models/Icon';
import { IconContainer } from '../styled_components/components/Icon';


function Icon(props: IconComponentProps) {
    const {
        onClick,
        ...restProps
    } = props;

    return (
        <IconContainer
            onClick={onClick}
            {...restProps} />
    );
}

export default Icon;