import React, { useState } from 'react';
import { useSpring, animated } from 'react-spring';

import { AppSidebar } from '../../styled_components';
import Icon from '../Icon';
import Divider from '../../components/Divider';
import MenuIcon from '../../assets/icons/menu.svg';
import './transitions.css';

function SideNav() {

    const [isToggled, setIsToggled] = useState(false);

    const sideBarSpring = useSpring({
        position: 'absolute',
        width: isToggled ? '128px' : '64px',
        height: '100%',
        top: 0,
        right: 0
    });

    return (
        <AppSidebar style={sideBarSpring}>
            <Icon
                onClick={() => setIsToggled(!isToggled)}
                imgUrl={MenuIcon} />
            <Divider
                orientation='horizontal'
                margin='10px 0'
                width='90%'
                height='1px'
                color='lightgrey' />
        </AppSidebar>
    );
}

export default SideNav;