import React from 'react';

import { DividerProps } from '../models/Divider';
import { StyledDivider } from '../styled_components/components/Divider';

function Divider(props: DividerProps) {
    return (
        <StyledDivider {...props} />
    );
}

export default Divider;