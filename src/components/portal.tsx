import { useEffect } from 'react';
import { createPortal } from 'react-dom';

import { ChildrenProp } from '../models';

type PortalProps = ChildrenProp & {
	type: 'notifications' | 'modals',
	open?: boolean
}

const rootElements = {
	modals: document.createElement('div'),
	notifications: document.createElement('div')
};


function Portal({ children, type, open }: PortalProps) {
	const modalRoot = document.getElementById(`portal-${type}`);
	
	useEffect(() => {
		if(open && modalRoot) {
			modalRoot.appendChild(rootElements[type]);
		} else if(!open && modalRoot && modalRoot.childNodes.length) {
			modalRoot.removeChild(rootElements[type]);
		}
		
		return () => {
			!open && modalRoot && modalRoot.childNodes.length && modalRoot.removeChild(rootElements[type]);
		};
	}, [ modalRoot, open, type ]);
	
	return createPortal(children, rootElements[type]);
}

export default Portal;
