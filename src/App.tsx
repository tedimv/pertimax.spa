import React from 'react';
import { Router, Route } from 'react-router';
import { createBrowserHistory } from 'history';

import AppThemeProvider from './theme/ThemeProvider';
import Main from './app/index';
import NotificationProvider from './contexts/notification';
import UserStore from './store';

const history = createBrowserHistory();

type AppProps = {
	store: typeof UserStore
}

const App = () => {
	
	return (
		<AppThemeProvider>
			<NotificationProvider>
				<Router history={history}>
					<Route
						path='/'
						component={Main} />
				</Router>
			</NotificationProvider>
		</AppThemeProvider>
	);
};

export default App;
