import { DtoAction, DtoActionTypes, DtoReader } from '../models/dto';

export function dtoBaseFactory<T>(init: T): DtoReader<T> {
	return (item?: T) => ({ ...init, ...item });
}

export function dtoBaseReducer<T>(defaultState: T) {
	return (state: T = defaultState, action: DtoAction<T, DtoActionTypes>): T => {
		const { payload, type, property } = action;
		
		switch (type) {
			case DtoActionTypes.update:
				return { ...state, [property]: payload };
			default :
				return state;
		}
	};
}
