import axios, { AxiosResponse } from 'axios';
import { TApiData, TAxiosPromise } from '../models/loader';
import { User } from '../models/user';
import { TEntityList } from '../models';

export function fetchUsers(): TAxiosPromise<User> {
	return axios.get('http://localhost:5000/users');
}

export function getUser(): TAxiosPromise<User> {
	return axios.get('http://localhost:5000/users/6032afc962c2ae513cb1989c');
}
