import axios from 'axios';

export const API_KEY = '1ad7ee4a-ac75-4818-a747-b55cf347c13f';

const queryURI = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest';

const params = {
	'start': '1',
	'limit': '5000',
	'convert': 'BGN'
};

const headers = {
	'X-CMC_PRO_API_KEY': API_KEY
};

export function fetchCrypto() {
	return axios.get(queryURI, { headers, params, responseType: 'json' });
}


