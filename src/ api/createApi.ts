import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

import { AuthState } from '../hooks/useAuth/reducer';
import { APP_URL } from './index';

export type ApiError = { message: string }
export type EntityList<T> = {
	entities: T[],
	total: number
}

export enum AxiosRequestType {
	get = 'get',
	post = 'post',
	put = 'put',
	delete = 'delete'
}

export type Api<T> = {
	url: string,
	get: ApiRequest<T>
	fetch: ApiRequest<T>
	post: ApiRequest<T>
	put: ApiRequest<T>
	delete: ApiRequest<T>
};
export type ApiRequest<T> = (data: T, params: AxiosRequestConfig) => Promise<AxiosResponse>;

function applyAuth(params: AxiosRequestConfig, hasAuth: boolean) {
	const foundAuth = localStorage.getItem('pertimax');
	if(!foundAuth || !hasAuth) return params;
	const auth: AuthState = JSON.parse(foundAuth);
	const result: AxiosRequestConfig = { ...params, headers: { Authorization: `Bearer ${auth.token}` } };
	return result;
}

function get<T>(url: string, id: string, params: AxiosRequestConfig): Promise<AxiosResponse<T | ApiError>> {
	return axios.get(url + `/${id}`, params);
}

function fetch<T>(url: string, params: AxiosRequestConfig): Promise<AxiosResponse<EntityList<T> | ApiError>> {
	return axios.get(url, params);
}

function post<T>(url: string, data: T, params: AxiosRequestConfig): Promise<AxiosResponse<T | ApiError>> {
	return axios.post(url, data, params);
}

function put<T>(url: string, data: T, params: AxiosRequestConfig): Promise<AxiosResponse<T | ApiError>> {
	return axios.put(url, data, params);
}

function del<T>(url: string, id: string, params: AxiosRequestConfig): Promise<AxiosResponse<T | ApiError>> {
	return axios.put(url + `/${id}`, params);
}

export function createRequest<T extends { _id: string }>(isPublic: boolean = false, url = '', method: AxiosRequestType, hasAuth = true): ApiRequest<T> {
	let wholeUri = isPublic ? url : APP_URL + url;
	
	return (data: T, params: AxiosRequestConfig = {}): Promise<AxiosResponse<T | T[]>> => {
		const args = [ applyAuth(params, hasAuth) ];
		if(method === 'put' || method === 'post') {
			// @ts-ignore
			args.unshift(data);
		} else {
			wholeUri += `/${data._id}`;
		}
		// @ts-ignore
		return axios[method](wholeUri, ...args);
	};
}

// export function createApi<T extends { _id: string }>(baseUrl: string, hasAuth: boolean = true): Api<T> {
//
// 	return {
// 		// get: createRequest<T>(false, baseUrl, AxiosRequestType.get, hasAuth),
// 		// fetch: createRequest<T>(false, baseUrl + '/all', AxiosRequestType.get),
// 		// post: createRequest<T>(false, baseUrl, AxiosRequestType.post),
// 		// put: createRequest<T>(false, baseUrl, AxiosRequestType.put),
// 		// delete: createRequest<T>(false, baseUrl, AxiosRequestType.delete),
// 		get,
// 		fetch,
// 		post,
// 		put,
// 		del,
// 		url: baseUrl
// 	};
// }

